# Disruptions  LimeSurvey Theme

This theme is based on the "Unity International LimeSurvey Theme" (https://gitlab.com/greater--good/unity-international-limesurvey-theme), which was in turn based on the theme for the "Your COVID-19 Risk" project (https://github.com/tammoterhark/ls3-covid-theme). That theme, in turn, was based on the Vanilla theme for LimeSurvey 3.x. The original Your COVID-19 Risk theme was developed by: Jan Ehrhardt (Tools for Research; javascript additions), Tammo ter Hark (Respondage; setup, styling), and Stefan Verweij (Evently; code review, add logo). The Unity International adaptions were implemented by Gjalt-Jorn Peter (Greater Good).

This version of the theme was developed by Hedde van Heerde and Martine Vecht.
